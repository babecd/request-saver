from elasticsearch import Elasticsearch
from datetime import datetime
import json

def datetime_serializer(obj):
    if isinstance(obj, datetime.datetime):
        serial = obj.isoformat()
        return serial
    return "Err: can't serialize"

class SaveApiWsgi(object):
    def __init__(self, app, kibana_prefix="logstash"):
        self.app = app
        self.kibana_prefix = kibana_prefix

    def __call__(self, environ, start_response):
        if environ['PATH_INFO'].startswith('/api/'):
            return self.is_api(environ, start_response)
        else:
            return self.app(environ, start_response)

    def is_api(self, environ, start_response):
        foo = self.app(environ, start_response)
        es = Elasticsearch()
        request_body_size = environ.get('CONTENT_LENGTH') or 0
        request_content_type = environ.get('CONTENT_TYPE', 'no type') or 'no type'
        request_path = environ.get('PATH_INFO', '/')
        method = environ.get('REQUEST_METHOD', 'None')
        if isinstance(request_body_size, int) and request_body_size != 0:
            data = environ['wsgi.input'].read(request_body_size)
        else:
            data = {}
        try:
            resp = json.dumps(foo.data, ensure_ascii=False, default=datetime_serializer)
        except AttributeError:
            resp = ""
        dt = datetime.now()
        doc = {
            'REQUEST_METHOD': method,
            'CONTENT_LENGTH': request_body_size,
            'CONTENT_TYPE': request_content_type,
            'PATH_INFO': request_path,
            'body': data,
            'response': resp,
            # 'sessionid': session,
            '@timestamp': dt.isoformat()
        }
        es.index(index=dt.strftime(self.kibana_prefix + "-%Y.%m.%d"), doc_type='request', body=doc, timestamp=dt.isoformat(), timeout=30)
        # for key, val in environ.items():
        #     print '%s = %s' % (key, val)
        return foo