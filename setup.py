from distutils.core import setup
setup(name='save_api_wsgi',
      version='1.0',
      packages=['save_api_wsgi'],
	  package_dir={'save_api_wsgi':'src/save_api_wsgi'},
	  install_requires=['elasticsearch'],
      )
